import * as ES6Promise from "es6-promise";
import Calculator from "./modules/Calculator";
import Translator from "./modules/Translator";
ES6Promise.polyfill();

function load() {
    const button = document.getElementById('calc');
    const translator = new Translator();

    if (button) {
        button.innerText = translator.trans('translated_title_for_button');
        button.addEventListener('click', async () => {
            await import('./modules/Calculator').then((calculator) => {
                const calc: Calculator = new calculator.default();
                console.log(calc.sum([1, 2, 3]));
            });
        });
    }
}

load();
