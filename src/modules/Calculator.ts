export default class Calculator {
    sum = (list: Array<number>) => {
        return list.reduce((a, b) => {
            return a + b;
        }, 0);
    }
};
